#!/usr/bin/ruby

require 'fileutils'

WORKING_DIR = Dir.home + "/Downloads"

Dir.open(WORKING_DIR).each do | file |
  if File.extname(file) == ".mp3"
    FileUtils.mv(WORKING_DIR + "/" + file, Dir.home() + "/Music")
    puts "Moving #{file}"
  end
end