class FIXNAME
  unless ARGV[0]
    puts "Usage: ruby fixname.rb  <pathtofile>"
    exit
  end

  old_filename = ARGV[0]

  unless File.exists?(old_filename)
    puts "Cannot find #{old_filname}. Please recheck file"
    exit
  end

  name = File.basename(old_filename, ".*")
  ext = File.extname(old_filename)
  replacements = { /;/ => "_",
                   /\s/ => "_",
                   /\'\`/ => "=",
                   /\&/ => "_and_",
                   /\$/ => "_dollar_",
                   /\%/ => "_percent_",
                   /[\(\)\[\]<>]/ => ""
  }

  replacements.each do |original, fix|
    name.gsub!(original,fix)
  end
  File.rename(old_filename, name + ext)
  puts "#{old_filename} --renamed--> #{name + ext}"
end
