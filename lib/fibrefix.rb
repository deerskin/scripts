#!/usr/bin/ruby

unless ARGV[0].to_s.end_with?(".mp3")
  puts "USAGE: fibrefix <path to Mp3>"
end

RAW_FILE = ARGV[0]
FILE_NAME = File.basename(RAW_FILE, ".mp3")
FILE_EXTENSION = File.extname(RAW_FILE)
PATH = File.dirname(RAW_FILE)

unless File.exists?(RAW_FILE)
  puts "Cannot find or access file #{RAW_FILE}"
end

fix_hash =  {
    /_/ => " ",
    "[www.MP3Fiber.com]" => "",
    "Download" => "",
    "Buy" => ""
}

begin
  puts "Renaming #{RAW_FILE}..."
  fix_hash.each do |unfixed, fixed|
    FILE_NAME.gsub!(unfixed, fixed)
  end
  fixed_name = (PATH + "/" + FILE_NAME + FILE_EXTENSION).to_s
  File.rename(RAW_FILE, fixed_name)
  puts "Renamed #{RAW_FILE} --> #{fixed_name}."
end